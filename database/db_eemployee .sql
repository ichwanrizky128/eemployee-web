-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2018 at 07:29 AM
-- Server version: 5.5.40
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_eemployee`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `nikdannid` varchar(10) NOT NULL,
  `namapegawai` varchar(50) NOT NULL,
  `programstudi` varchar(50) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `ikatan` varchar(50) NOT NULL,
  `jeniskelamin` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kotalahir` varchar(20) NOT NULL,
  `tgllahir` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nikdannid`, `namapegawai`, `programstudi`, `pendidikan`, `jabatan`, `ikatan`, `jeniskelamin`, `alamat`, `telp`, `email`, `kotalahir`, `tgllahir`, `foto`, `level`) VALUES
('3311611002', 'Alfikri Somar Ziggi', 'Teknik Informatika', 'S-3', 'Asisten Ahli', 'Dosen Paruh Waktu', 'Perempuan', 'Bengkong, Batam Kepulauan Riau', '08xxxxxx', 'alfikrisomar@polibatam.com', 'padang', '2018-05-03', '3311611002.PNG', 'Dosen'),
('3311611018', 'Mohammad Ichwan Rizky Kurnia', 'Teknik Informatika ', 'D-4', 'Kepala Kepegawaian', 'Pegawai Paruh Waktu', 'Laki-Laki', 'Batu aji', '08117779914', 'ichwanrizky128@polibatam.ac.id', 'Batam', '1997-09-13', '3311611018.PNG', 'Staff'),
('3311611020', 'Muhammad Zulfikar', 'Multimedia dan Jaringan', 'S-1', 'Tenaga Ahli', 'Dosen Tetap', 'Laki-Laki', 'Bengkong dibatam', '08xxxxx', 'muhammadzulf@polibatam.ac.id', 'Batam', '2018-05-03', '3311611020.PNG', 'Dosen'),
('3311611024', 'Karolina Wilanda', 'Teknik Informatika ', 'D-4', 'Kepala Administrasi Keuangan', 'Pegawai Paruh Waktu', 'Perempuan', 'Batam', '082273853743', 'olin@polibatam.ac.id', 'batam', '1999-05-05', '3311611024.PNG', 'Staff'),
('admin001', 'Mohammad Ichwan Rizky Kurnia', '', '', 'Staff Kepegawaian', 'Pegawai Paruh Waktu', 'Laki-Laki', 'Batam', '08117779914', 'ichwanrizky128@polibatam.ac.id', 'Batam', '1997-09-13', '3311611018.PNG', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_user`
--

CREATE TABLE IF NOT EXISTS `pegawai_user` (
  `nikdannid` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai_user`
--

INSERT INTO `pegawai_user` (`nikdannid`, `nama`, `password`, `level`, `foto`) VALUES
('3311611002', 'Alfikri Somar Ziggi', 'e2b7953c2d21cc797ef8eb5964fc93e9', 'Dosen', '3311611002.PNG'),
('3311611018', 'Mohammad Ichwan Rizky Kurnia', 'c59f63db118252b5933653295a6eabe5', 'Staff', '3311611018.PNG'),
('3311611020', 'Muhammad Zulfikar', '9ad8dfc23cb478abd7fd2e17b9552ab2', 'Dosen', '3311611020.PNG'),
('3311611024', 'Karolina Wilanda', '258d81000838122fa1ff4b68b4498df8', 'Staff', '3311611024.PNG'),
('admin001', 'Mohammad Ichwan Rizky Kurnia', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '3311611002.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `pjabatan`
--

CREATE TABLE IF NOT EXISTS `pjabatan` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jpengajuan` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `jabatandiajukan` varchar(50) NOT NULL,
  `tglpengajuan` varchar(10) NOT NULL,
  `level` varchar(10) NOT NULL,
  `file` varchar(50) NOT NULL,
  `persetujuan` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pjabatan`
--

INSERT INTO `pjabatan` (`id`, `nik`, `nama`, `jpengajuan`, `jabatan`, `jabatandiajukan`, `tglpengajuan`, `level`, `file`, `persetujuan`, `foto`) VALUES
(53, '3311611002', 'Alfikri Somar Ziggi', 'Pengajuan Pangkat Jabatan', 'Tenaga Ahli', 'Asisten Ahli', '07-06-2018', 'Dosen', 'file.rar', 'Setuju', '3311611002.PNG'),
(54, '3311611002', 'Alfikri Somar Ziggi', 'Pengajuan Pangkat Jabatan', 'Asisten Ahli', 'Lector', '07-06-2018', 'Dosen', 'file.rar', 'Belum Disetujui', '3311611002.PNG'),
(55, '3311611020', 'Muhammad Zulfikar', 'Pengajuan Pangkat Jabatan', 'Tenaga Ahli', 'Asisten Ahli', '07-06-2018', 'Dosen', 'file.rar', 'Belum Disetujui', '3311611020.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `ppelatihan`
--

CREATE TABLE IF NOT EXISTS `ppelatihan` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jpengajuan` varchar(50) NOT NULL,
  `tglpengajuan` varchar(10) NOT NULL,
  `namapelatihan` varchar(50) NOT NULL,
  `tgldimulai` varchar(10) NOT NULL,
  `tglselesai` varchar(10) NOT NULL,
  `level` varchar(10) NOT NULL,
  `file` varchar(50) NOT NULL,
  `persetujuan` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppelatihan`
--

INSERT INTO `ppelatihan` (`id`, `nik`, `nama`, `jpengajuan`, `tglpengajuan`, `namapelatihan`, `tgldimulai`, `tglselesai`, `level`, `file`, `persetujuan`, `foto`) VALUES
(11, '3311611020', 'Muhammad Zulfikar', 'Pengajuan Pelatihan', '07-06-2018', 'Pelatihan Networking', '2018-06-08', '2018-06-09', 'Dosen', 'file.rar', 'Setuju', '3311611020.PNG'),
(12, '3311611020', 'Muhammad Zulfikar', 'Pengajuan Pelatihan', '07-06-2018', 'Pelatihan Networking 2', '2018-06-25', '2018-06-28', 'Dosen', 'file.rar', 'Setuju', '3311611020.PNG'),
(13, '3311611002', 'Alfikri Somar Ziggi', 'Pengajuan Pelatihan', '07-06-2018', 'Pelatihan CISCO', '2018-06-14', '2018-06-16', 'Dosen', 'file.rar', 'Setuju', '3311611002.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `ppendidikan`
--

CREATE TABLE IF NOT EXISTS `ppendidikan` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jpengajuan` varchar(50) NOT NULL,
  `tglpengajuan` varchar(10) NOT NULL,
  `pendterakhir` varchar(10) NOT NULL,
  `programstudi` varchar(50) NOT NULL,
  `penddiajukan` varchar(10) NOT NULL,
  `programstudidiajukan` varchar(50) NOT NULL,
  `namauniversitas` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  `file` varchar(50) NOT NULL,
  `persetujuan` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppendidikan`
--

INSERT INTO `ppendidikan` (`id`, `nik`, `nama`, `jpengajuan`, `tglpengajuan`, `pendterakhir`, `programstudi`, `penddiajukan`, `programstudidiajukan`, `namauniversitas`, `level`, `file`, `persetujuan`, `foto`) VALUES
(24, '3311611020', 'Muhammad Zulfikar', 'Pengajuan Pendidikan', '07-06-2018', 'S-1', 'Multimedia dan Jaringan', 'S-2', 'Multimedia dan Jaringan', 'Universitas Indonesia', 'Dosen', 'file.rar', 'Setuju', '3311611020.PNG'),
(25, '3311611002', 'Alfikri Somar Ziggi', 'Pengajuan Pendidikan', '07-06-2018', 'S-2', 'Teknik Informatika', 'S-3', 'Teknik Informatika', 'Universitas Indonesia', 'Dosen', 'file.rar', 'Setuju', '3311611002.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `pstatus`
--

CREATE TABLE IF NOT EXISTS `pstatus` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jpengajuan` varchar(50) NOT NULL,
  `tglpengajuan` varchar(10) NOT NULL,
  `ikatan` varchar(50) NOT NULL,
  `ikatandiajukan` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  `file` varchar(50) NOT NULL,
  `persetujuan` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jabatan`
--

CREATE TABLE IF NOT EXISTS `t_jabatan` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatansebelumnya` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `tgldisetujui` varchar(10) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jabatan`
--

INSERT INTO `t_jabatan` (`id`, `nik`, `nama`, `jabatansebelumnya`, `jabatan`, `tgldisetujui`, `foto`) VALUES
(21, '3311611002', 'Alfikri Somar Ziggi', 'Tenaga Ahli', 'Asisten Ahli', '07-06-2018', '3311611002.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `t_pelatihan`
--

CREATE TABLE IF NOT EXISTS `t_pelatihan` (
  `id` int(10) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `namapelatihan` varchar(50) NOT NULL,
  `tgldimulai` varchar(10) NOT NULL,
  `tglselesai` varchar(10) NOT NULL,
  `sertifikat` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pelatihan`
--

INSERT INTO `t_pelatihan` (`id`, `nik`, `nama`, `namapelatihan`, `tgldimulai`, `tglselesai`, `sertifikat`, `foto`) VALUES
(15, '3311611002', 'Alfikri Somar Ziggi', 'Pelatihan CISCO', '2018-06-14', '2018-06-16', '', '3311611002.PNG'),
(16, '3311611020', 'Muhammad Zulfikar', 'Pelatihan Networking 2', '2018-06-25', '2018-06-28', '', '3311611020.PNG'),
(17, '3311611020', 'Muhammad Zulfikar', 'Pelatihan Networking', '2018-06-08', '2018-06-09', '', '3311611020.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `t_pendidikan`
--

CREATE TABLE IF NOT EXISTS `t_pendidikan` (
  `id` int(10) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `penddiajukan` varchar(10) NOT NULL,
  `programstudidiajukan` varchar(50) NOT NULL,
  `namauniversitas` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pendidikan`
--

INSERT INTO `t_pendidikan` (`id`, `nik`, `nama`, `penddiajukan`, `programstudidiajukan`, `namauniversitas`, `foto`, `status`) VALUES
(10, '3311611002', 'Alfikri Somar Ziggi', 'S-3', 'Teknik Informatika', 'Universitas Indonesia', '3311611002.PNG', 'Telah Selesai'),
(11, '3311611020', 'Muhammad Zulfikar', 'S-2', 'Multimedia dan Jaringan', 'Universitas Indonesia', '3311611020.PNG', 'Sedang Berlangsung');

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE IF NOT EXISTS `t_status` (
  `id` int(4) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ikatansebelumnya` varchar(50) NOT NULL,
  `ikatan` varchar(50) NOT NULL,
  `tgldisetujui` varchar(10) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nikdannid`), ADD UNIQUE KEY `nikdannid` (`nikdannid`), ADD KEY `namadosen` (`namapegawai`), ADD KEY `proramstudi` (`programstudi`), ADD KEY `pendidikan` (`pendidikan`), ADD KEY `jabatan` (`jabatan`), ADD KEY `ikatan` (`ikatan`), ADD KEY `jeniskelamin` (`jeniskelamin`), ADD KEY `kotalahir` (`kotalahir`), ADD KEY `tgllahir` (`tgllahir`), ADD KEY `namadosen_2` (`namapegawai`), ADD KEY `programstudi` (`programstudi`), ADD KEY `pendidikan_2` (`pendidikan`), ADD KEY `jabatan_2` (`jabatan`), ADD KEY `ikatan_2` (`ikatan`), ADD KEY `jeniskelamin_2` (`jeniskelamin`), ADD KEY `alamat` (`alamat`), ADD KEY `telp` (`telp`), ADD KEY `email` (`email`), ADD KEY `kotalahir_2` (`kotalahir`), ADD KEY `tgllahir_2` (`tgllahir`), ADD KEY `level` (`level`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `pegawai_user`
--
ALTER TABLE `pegawai_user`
  ADD PRIMARY KEY (`nikdannid`), ADD KEY `nama` (`nama`), ADD KEY `level` (`level`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `pjabatan`
--
ALTER TABLE `pjabatan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `level` (`level`), ADD KEY `jpengajuan` (`jpengajuan`), ADD KEY `jabatan` (`jabatan`), ADD KEY `level_2` (`level`), ADD KEY `persetujuan` (`persetujuan`), ADD KEY `file` (`file`), ADD KEY `jabatandiajukan` (`jabatandiajukan`), ADD KEY `jabatan_2` (`jabatan`), ADD KEY `jpengajuan_2` (`jpengajuan`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `ppelatihan`
--
ALTER TABLE `ppelatihan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `level` (`level`), ADD KEY `namapelatihan` (`namapelatihan`), ADD KEY `tgldimulai` (`tgldimulai`), ADD KEY `tglselesai` (`tglselesai`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `ppendidikan`
--
ALTER TABLE `ppendidikan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `pendterakhir` (`pendterakhir`), ADD KEY `level` (`level`), ADD KEY `foto` (`foto`), ADD KEY `programstudi` (`programstudi`);

--
-- Indexes for table `pstatus`
--
ALTER TABLE `pstatus`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `level` (`level`), ADD KEY `ikatan` (`ikatan`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `foto` (`foto`), ADD KEY `nama` (`nama`);

--
-- Indexes for table `t_pelatihan`
--
ALTER TABLE `t_pelatihan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `namapelatihan` (`namapelatihan`), ADD KEY `tgldimulai` (`tgldimulai`), ADD KEY `tglselesai` (`tglselesai`), ADD KEY `nik_2` (`nik`), ADD KEY `nama_2` (`nama`), ADD KEY `namapelatihan_2` (`namapelatihan`), ADD KEY `tgldimulai_2` (`tgldimulai`), ADD KEY `tglselesai_2` (`tglselesai`), ADD KEY `nik_3` (`nik`), ADD KEY `nama_3` (`nama`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `foto` (`foto`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`id`), ADD KEY `nik` (`nik`), ADD KEY `nama` (`nama`), ADD KEY `foto` (`foto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pjabatan`
--
ALTER TABLE `pjabatan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `ppelatihan`
--
ALTER TABLE `ppelatihan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ppendidikan`
--
ALTER TABLE `ppendidikan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `pstatus`
--
ALTER TABLE `pstatus`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `t_pelatihan`
--
ALTER TABLE `t_pelatihan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pegawai_user`
--
ALTER TABLE `pegawai_user`
ADD CONSTRAINT `userfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `usernama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `usernik` FOREIGN KEY (`nikdannid`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pjabatan`
--
ALTER TABLE `pjabatan`
ADD CONSTRAINT `pjabatanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pjabatannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pjabatannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ppelatihan`
--
ALTER TABLE `ppelatihan`
ADD CONSTRAINT `ppelatihanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ppelatihannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ppelatihannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ppendidikan`
--
ALTER TABLE `ppendidikan`
ADD CONSTRAINT `ppendidikanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ppendidikannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ppendidikannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pstatus`
--
ALTER TABLE `pstatus`
ADD CONSTRAINT `pstatusfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pstatusnama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pstatusnik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_jabatan`
--
ALTER TABLE `t_jabatan`
ADD CONSTRAINT `t_jabatannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_jabatanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_jabatannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_pelatihan`
--
ALTER TABLE `t_pelatihan`
ADD CONSTRAINT `pelatihanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pelatihannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pelatihannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
ADD CONSTRAINT `pendidikanfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pendidikannama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pendidikannik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_status`
--
ALTER TABLE `t_status`
ADD CONSTRAINT `t_statusfoto` FOREIGN KEY (`foto`) REFERENCES `pegawai` (`foto`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_statusnama` FOREIGN KEY (`nama`) REFERENCES `pegawai` (`namapegawai`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `t_statusnik` FOREIGN KEY (`nik`) REFERENCES `pegawai` (`nikdannid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
