<?php 
	include '../conn/koneksi.php';
	
	$nikdannid		= $_GET['nikdannid'];
	
	$query = "SELECT * FROM pegawai WHERE nikdannid='$nikdannid'";
	$sql = mysql_query($query);
	$data = mysql_fetch_array($sql);
	$nikdannid = $data['nikdannid'];
	$namapegawai = $data['namapegawai'];
	$programstudi = $data['programstudi'];
	$pendidikan = $data['pendidikan'];
	$jabatan	= $data['jabatan'];
	$ikatan = $data['ikatan'];
	$jeniskelamin = $data['telp'];
	$alamat = $data['alamat'];
	$telp = $data['telp'];
	$email = $data['email'];
	$kotalahir = $data['kotalahir'];
	$tgllahir = $data['tgllahir'];
	$foto = $data['foto'];
	$level = $data['level'];
?>

<!-- menu tengah -->
	<div id="menu-tengah">
    	<div id="bg_menu">Data Pegawai
    	</div>
    	<div id="content_menu">
        <div id="menu_header">
        	<table width="100%" height="100%" style="background-color:#9cc;">
            	<tr>
                	<td align="center">Edit Data Pegawai</td>
                </tr>
            </table>
            
    	</div>
   	    <div class="table_input">
        <form action="?page=pegawai_proses_edit&nikdannid=<?php echo $data['nikdannid'] ?>" method="post">
        <input type="hidden" name="nik" value="<?php echo $nikdannid; ?>"></td>
   	      <table width="100%" height="80%" align="center" cellspacing="0" cellpadding="5">
   	        <tbody>
            	<tr>
                	<td width="25%" align="right">NIK / NID</td>
                    <td><input type="text" name="nikdannid" size="50%" value="<?=$nikdannid?>"></td>
                </tr>
                <tr>
                	<td width="20%" align="right">Nama</td>
                    <td><input type="text" name="namapegawai" size="50%" value="<?=$namapegawai?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Program Studi</td>
                    <td><input type="text" name="programstudi" size="50%" value="<?=$programstudi?>"></td>
                </tr>
                <tr>              
				<tr>
                	<td width="20%" align="right">Pendidikan</td>
                    <td><select name="pendidikan">
                    		<option value="">Pilih Pendidikan</option>                            
                            <option value="SMA/SMK" <?php if( $pendidikan=='SMA/SMK'){echo "selected"; } ?>>SMA/SMK</option>
                            <option value="S-1" <?php if( $pendidikan=='S-1'){echo "selected"; } ?>>S-1</option>
							<option value="S-2" <?php if( $pendidikan=='S-2'){echo "selected"; } ?>>S-2</option>
							<option value="D-3" <?php if( $pendidikan=='D-3'){echo "selected"; } ?>>D-3</option>
							<option value="D-4" <?php if( $pendidikan=='D-4'){echo "selected"; } ?>>D-4</option>
						</select>
                    </td>
                </tr>  
				<tr>
                	<td width="20%" align="right">Jabatan</td>
                    <td><input type="text" name="jabatan" size="50%" value="<?=$jabatan?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Ikatan</td>
                    <td><input type="text" name="ikatan" size="50%" value="<?=$ikatan?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Jenis Kelamin</td>
                    <td><?php if ($data['jeniskelamin'] === "Laki-Laki") : ?>
                    <input type="radio" name="jeniskelamin" value="Laki-Laki" checked />Laki-Laki</label>
            		<input type="radio" name="jeniskelamin" value="Perempuan" />Perempuan</label>
                    <?php else : ?>
            		<input type="radio" name="jeniskelamin" value="Laki-Laki" />Laki-Laki</label>
            		<input type="radio" name="jeniskelamin" value="Perempuan" checked />Perempuan</label>
            		<?php endif; ?>
                </tr>
				<tr>
                	<td width="20%" align="right">Alamat</td>
                    <td><input type="text" name="alamat" size="50%" value="<?=$alamat?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Telp</td>
                    <td><input type="text" name="telp" size="50%" value="<?=$telp?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Email</td>
                    <td><input type="text" name="email" size="50%" value="<?=$email?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Kota Lahir</td>
                    <td><input type="text" name="kotalahir" size="50%" value="<?=$kotalahir?>"></td>
                </tr> 
				<tr>
                	<td width="20%" align="right">Tanggal Lahir</td>
                    <td><input type="date" name="tgllahir" value="<?=$tgllahir?>"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Level</td>
                    <td>					
					<?php if ($data['level'] === "Admin") : ?>
					<input type="radio" name="level" value="Admin" checked />Admin</label>
            		<input type="radio" name="level" value="Dosen" />Dosen</label>
					<input type="radio" name="level" value="Staff" />Staff</label>
					<?php elseif ($data['level'] === "Dosen") : ?>
					<input type="radio" name="level" value="Admin" />Admin</label>
            		<input type="radio" name="level" value="Dosen" checked />Dosen</label>
					<input type="radio" name="level" value="Staff" />Staff</label>
                    <?php else : ?>
					<input type="radio" name="level" value="Admin" />Admin</label>
            		<input type="radio" name="level" value="Dosen" />Dosen</label>
					<input type="radio" name="level" value="Staff" checked />Staff</label>
            		<?php endif; ?>
                </tr>				
                <tr>
                	<td></td>
                    <td><input type="submit" value="Simpan"></td>
                </tr>
                <tr>
                    <td><a href="?page=pegawai">Batal
                    </a></td>
                </tr>
            
            </tbody>
          </table>
          </form>
 	      </div>
   	  </div>
    </div>