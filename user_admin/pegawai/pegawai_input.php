<!-- menu tengah -->
	<div id="menu-tengah">
    	<div id="bg_menu">Data Pegawai
    	</div>
    	<div id="content_menu">
        <div id="menu_header">
        	<table width="100%" height="100%" style="background-color:#9cc;">
            	<tr>
                	<td align="center">Input Data Pegawai</td>
                </tr>
            </table>
            
    	</div>
   	    <div class="table_input">
        <form action="?page=pegawai_proses" method="post">
        <input type="hidden" name="tgl_input" value="<?php echo $waktu; ?>">
   	      <table width="100%" height="80%" align="center" cellspacing="0" cellpadding="5">
   	        <tbody>
            	<tr>
                	<td width="25%" align="right">NIK / NID</td>
                    <td><input type="text" name="nikdannid" size="50%" required="required"></td>
                </tr>
                <tr>
                	<td width="20%" align="right">Nama</td>
                    <td><input type="text" name="namapegawai" size="50%" required="required"></td>
                </tr>				
                <tr>
                	<td width="20%" align="right">Pendidikan</td>
                    <td><select name="pendidikan">
                    		<option value="">Pilih Pendidikan</option>                            
                            <option value="SMA/SMK">SMA/SMK</option>
                            <option value="S-1">S-1</option>
							<option value="S-2">S-2</option>
							<option value="D-3">D-3</option>
							<option value="D-4">D-4</option>
						</select>
						Program Studi
						<input type="text" name="programstudi" size="21%">
                    </td>
					
                </tr>   
				<tr>
                	<td width="20%" align="right">Jabatan</td>
                    <td><input type="text" name="jabatan" size="50%" required="required"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Ikatan</td>
                    <td><input type="text" name="ikatan" size="50%" required="required"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Jenis Kelamin</td>
                    <td>
                    <input type="radio" name="jeniskelamin" value="Laki-laki"/>Laki-laki
   					<input type="radio" name="jeniskelamin" value="Perempuan"/>Perempuan
                    </td>
                </tr>
				<tr>
                	<td width="20%" align="right">Alamat</td>
                    <td><input type="text" name="alamat" size="50%" required="required"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Telp</td>
                    <td><input type="text" name="telp" size="50%" required="required"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Email</td>
                    <td><input type="text" name="email" size="50%" required="required"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Kota Lahir</td>
                    <td><input type="text" name="kotalahir" size="50%" required="required"></td>
                </tr> 
				<tr>
                	<td width="20%" align="right">Tanggal Lahir</td>
                    <td><input type="date" name="tgllahir"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Foto</td>
                    <td><img src="" width="80" height="100"><br>
                    <input type="file" name="foto"><input type="text" name="foto" size="50%"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Level</td>
                    <td>
                    <input type="radio" name="level" value="Dosen"/>Dosen
   					<input type="radio" name="level" value="Staff"/>Staff
                    </td>
                </tr>
                <tr>
                	<td></td>
                    <td><input type="submit" value="Simpan"></td>
                </tr>
                <tr>
                    <td><a href="?page=pegawai">Kembali</td>
                </tr>
            
            </tbody>
          </table>
          </form>
 	      </div>
   	  </div>
    </div>