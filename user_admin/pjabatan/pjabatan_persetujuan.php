<?php 
	include '../conn/koneksi.php';
	$tanggal = date('Y-m-d');
	$jam = date('H:i:s');
	$waktu = $tanggal.' '.$jam;
	
	$nik1		= $_GET['id'];
	
	$query = "SELECT * FROM pjabatan WHERE id=$nik1";
	$sql = mysql_query($query);
	$data = mysql_fetch_array($sql);
	$id = $data['id'];
	$nik = $data['nik'];
	$nama = $data['nama'];
	$jpengajuan	 = $data['jpengajuan'];
	$jabatan	 = $data['jabatan'];
	$jabatandiajukan	 = $data['jabatandiajukan'];
	$tglpengajuan = $data['tglpengajuan'];
	$level = $data['level'];
	$file = $data['file'];
	$foto = $data['foto'];
	$persetujuan = $data['persetujuan'];
?>

<!-- menu tengah -->
	<div id="menu-tengah">
    	<div id="bg_menu">Data Pengajuan Jabatan
    	</div>
    	<div id="content_menu">
        <div id="menu_header">
        	<table width="100%" height="100%" style="background-color:#9cc;">
            	<tr>
                	<td align="center">Detail Pengajuan</td>
                </tr>
            </table>
            
    	</div>
   	    <div class="table_input">
        <form action="?page=pjabatan_persetujuan_proses" method="post">
        <input type="hidden" name="id" value="<?=$id ?>"></td>
		<input type="hidden" name="foto" value="<?=$foto ?>"></td>
   	      <table width="100%" height="80%" align="center" cellspacing="0" cellpadding="5">
   	        <tbody>
            	<tr>
                	<td width="20%" align="right">NIK / NID</td>
                    <td><input type="text" name="nik" size="50%" value="<?=$nik ?>" readonly="readonly"></td>
                </tr>
                <tr>
                	<td width="20%" align="right">Nama</td>
                    <td><input type="text" name="nama" size="50%" value="<?=$nama ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Jenis Pengajuan</td>
                    <td><input type="text" name="jpengajuan" size="50%" value="<?=$jpengajuan ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Jabatan</td>
                    <td><input type="text" name="jabatan" size="50%" value="<?=$jabatan ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Jabatan Diajukan</td>
                    <td><input type="text" name="jabatandiajukan" size="50%" value="<?=$jabatandiajukan ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Tanggal Pengajuan</td>
                    <td><input type="text" name="tglpengajuan" size="50%" value="<?=$tglpengajuan ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Level</td>
                    <td><input type="text" name="level" size="50%" value="<?=$level ?>" readonly="readonly"></td>
                </tr>
				<tr>
                	<td width="20%" align="right">File</td>
                    <td><a href="../file/<?=$file ?>"><?=$file ?></a></td>
                </tr>
				<tr>
                	<td width="20%" align="right">Persetujuan</td>
                    <td>
					<?php if ($persetujuan === "Setuju") : ?>
					<input type="radio" name="persetujuan" value="Setuju" checked />Setuju</label>
            		<input type="radio" name="persetujuan" value="Tidak Setuju" />Tidak Setuju</label>
					<input type="radio" name="persetujuan" value="Belum Disetujui" />Belum Disetujui</label>
					<?php elseif ($persetujuan === "Tidak Setuju") : ?>
					<input type="radio" name="persetujuan" value="Setuju" />Setuju</label>
            		<input type="radio" name="persetujuan" value="Tidak Setuju" checked />Tidak Setuju</label>
					<input type="radio" name="persetujuan" value="Belum Disetujui" />Belum Disetujui</label>
                    <?php else : ?>
					<input type="radio" name="persetujuan" value="Setuju" />Setuju</label>
            		<input type="radio" name="persetujuan" value="Tidak Setuju" />Tidak Setuju</label>
					<input type="radio" name="persetujuan" value="Belum Disetujui" checked />Belum Disetujui</label>
            		<?php endif; ?>
                </tr>		
                <tr>
                	<td></td>
                    <td width="20%"><input type="submit" value="Simpan"></td>
                </tr>
                <tr>
                    <td><a href="?page=pjabatan">Batal</a>
					</td>
                </tr>
            
            </tbody>
          </table>
          </form>
 	      </div>
   	  </div>
    </div>