<?php
session_start();
if(!isset($_SESSION['nama'])){
	echo "<script>alert('Silahkan login terlebih dahulu')</script>";
	echo "<meta http-equiv='refresh' content='0; url=../index.php'>";
} else {

?>
<!DOCTYPE html> 
<html lang="en"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>E - Employee </title>
<link rel="stylesheet" type="text/css" href="../images/style.css" />
<link rel="shortcut icon" href="../poltek.jpg" />
</head>

<body>
<!-- menu main sebagai div Utama -->

<div id="main">
	<!-- menu Header -->
    <div id="header">
    <img src="../images/header.jpg" />
    </div>
    
    <!-- menu Header -->
    <div id="menu-atas">
    	<div id="menu_user">
        <span><?=$_SESSION['nama'];?> (Staff) </span>
        </div>
        <div id="menu_tanggal" align="right">
        <span><?php
		 	$array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
 			$hr = $array_hr[date('N')];
			$tgl= date('j');
			$array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
			$bln = $array_bln[date('n')];
			$thn = date('Y');
			echo $hr . ", " . $tgl . " " . $bln . " " . $thn . " ";
			?>
        </span>
        </div>
    </div>
    
    
<div>    
    <!-- menu Kiri -->
 	<div id="menu-kiri">
    	<div id="bg_menu">Menu Utama
    	</div>
    	<div id="left_menu">
        	<a href="index.php" class="menu">&raquo; Home </a> <br />
        	<a href="?page=pegawai" class="menu">&raquo; Data Pegawai </a> <br />
			<a href="index.php" class="menu">&raquo; Data Pengajuan </a> <br />
			<a href="?page=riwayat" class="menu">&raquo; Riwayat </a> <br />
        	<a href="?page=ubahpassword&nikdannid=<?=$_SESSION['nikdannid'];?>" class="menu">&raquo; Ubah Password </a> <br />
            <a href="../logout.php" class="menu">&raquo; Logout </a> <br />
        </div>
    </div>
	    
    	     <?php
				 error_reporting(0);
				 switch($_GET['page'])
				 	{
						default:
						include "home.php";
						break;
						
						// menu pegawai				
						case "pegawai";
						include "pegawai/pegawai_data.php";
						break;
						case "detil-pegawai";
						include "pegawai/pegawai_detil.php";
						break;
						case "pegawai_input";
						include "pegawai/pegawai_input.php";
						break;
						case "pegawai_proses";
						include "pegawai/pegawai_proses.php";
						break;
						case "pegawai_search";
						include "pegawai/pegawai_search.php";
						break;
						case "pegawai_edit";
						include "pegawai/pegawai_edit.php";
						break;
						case "pegawai_proses_edit";
						include "pegawai/pegawai_proses_edit.php";
						break;
						case "pegawai_hapus";
						include "pegawai/pegawai_hapus.php";
						break;
						
						// ubah password
						case "ubahpassword";
						include "ubahpassword/ubahpassword.php";
						break;
						case "ubahpassword_proses";
						include "ubahpassword/ubahpassword_proses.php";
						break;		

						// Form Pengajuan Pangkat Jabatan
						case "pjabatan";
						include "pjabatan/pjabatan.php";
						break;
						case "pjabatan_input";
						include "pjabatan/pjabatan_input.php";
						break;
						case "pjabatan_proses";
						include "pjabatan/pjabatan_proses.php";
						break;
						case "pjabatan_detil";
						include "pjabatan/pjabatan_detil.php";
						break;
						case "pjabatan_search";
						include "pjabatan/pjabatan_search.php";
						break;
						
						// Form Pengajuan Pelatihan
						case "ppelatihan";
						include "ppelatihan/ppelatihan.php";
						break;
						case "ppelatihan_input";
						include "ppelatihan/ppelatihan_input.php";
						break;
						case "ppelatihan_proses";
						include "ppelatihan/ppelatihan_proses.php";
						break;
						case "ppelatihan_detil";
						include "ppelatihan/ppelatihan_detil.php";
						break;
						case "ppelatihan_search";
						include "ppelatihan/ppelatihan_search.php";
						break;
						
						// Form Pengajuan Perubahan Status
						case "pstatus";
						include "pstatus/pstatus.php";
						break;
						case "pstatus_input";
						include "pstatus/pstatus_input.php";
						break;
						case "pstatus_proses";
						include "pstatus/pstatus_proses.php";
						break;
						case "pstatus_detil";
						include "pstatus/pstatus_detil.php";
						break;
						case "pstatus_search";
						include "pstatus/pstatus_search.php";
						break;
						
						// Form Pengajuan Pendidikan
						case "ppendidikan";
						include "ppendidikan/ppendidikan.php";
						break;
						case "ppendidikan_input";
						include "ppendidikan/ppendidikan_input.php";
						break;
						case "ppendidikan_proses";
						include "ppendidikan/ppendidikan_proses.php";
						break;
						case "ppendidikan_detil";
						include "ppendidikan/ppendidikan_detil.php";
						break;
						case "ppendidikan_search";
						include "ppendidikan/ppendidikan_search.php";
						break;
						
						// Riwayat
						case "riwayat";
						include "riwayat/riwayat.php";
						break;
						
						case "riwayat_jabatan";
						include "riwayat_jabatan/riwayat_jabatan.php";
						break;
						case "riwayat_jabatan_hapus";
						include "riwayat_jabatan/riwayat_jabatan_hapus.php";
						break;
						case "riwayat_jabatan_search";
						include "riwayat_jabatan/riwayat_jabatan_search.php";
						break;
						
						case "riwayat_pelatihan";
						include "riwayat_pelatihan/riwayat_pelatihan.php";
						break;
						case "riwayat_pelatihan_hapus";
						include "riwayat_pelatihan/riwayat_pelatihan_hapus.php";
						break;
						case "riwayat_pelatihan_search";
						include "riwayat_pelatihan/riwayat_pelatihan_search.php";
						break;
						
						case "riwayat_pendidikan";
						include "riwayat_pendidikan/riwayat_pendidikan.php";
						break;
						case "riwayat_pendidikan_hapus";
						include "riwayat_pendidikan/riwayat_pendidikan_hapus.php";
						break;
						case "riwayat_pendidikan_search";
						include "riwayat_pendidikan/riwayat_pendidikan_search.php";
						break;
						
						case "riwayat_status";
						include "riwayat_status/riwayat_status.php";
						break;
						case "riwayat_status_hapus";
						include "riwayat_status/riwayat_status_hapus.php";
						break;
						case "riwayat_status_search";
						include "riwayat_status/riwayat_status_search.php";
						break;
					}
			?>

    
</div>
    <!-- menu Merapikan div content -->
    <div class="clear">
   	</div>
    
  	<!-- menu Footer -->
    <div id="footer"><center>POLITEKNIK NEGERI BATAM 2018</center></div>
    
</div>

</body>
</html>

<?php } ?>